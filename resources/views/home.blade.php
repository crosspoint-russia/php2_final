@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12"><h1>Публикации</h1></div>
</div>


    <div class="container">
        @foreach ($posts as $post)
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><a href="/show/{{ $post->id }}">{{ $post->title }}</a></div>

                        <div class="panel-body">
                            {{ str_limit($post->text, 450) }}
                        </div>

                        <div class="panel-footer">
                            @if(!$post->user()->trashed())
                            <p>Опубликовал: <a href="/user/{{ $post->user()->id }}">{{ $post->user()->name }}</a></p>
                            @else
                            <p>Автор публикации <s>{{ $post->user()->name }}</s> покинул ресурс в добровольно-принудительном порядке</p>
                            @endif
                            <p>Дата публикации: {{ $post->created_at }}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="row"><div class="col-md-12">{{ $posts->links() }}</div></div>


    </div>

@endsection
