@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12"><h1>Публикации пользователя {{ $user->name }}</h1></div>
    </div>


    <div class="container">
        @foreach ($posts as $post)
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><a href="/show/{{ $post->id }}">{{ $post->title }}</a></div>

                        <div class="panel-body">
                            {{ $post->text }}
                        </div>

                        <div class="panel-footer">
                            <p>Дата публикации: {{ $post->created_at }}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach



    </div>

@endsection