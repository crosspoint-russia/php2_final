@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{ $post->title }}</h1></div>

                <div class="panel-body">
                    {{ $post->text }}
                </div>

                <div class="panel-footer">
                    <p>Опубликовал: <a href="/user/{{ $post->user()->id }}">{{ $post->user()->name }}</a></p>
                    <p>Дата публикации: {{ $post->created_at }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

@if(Auth::check())
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="/comment/add">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name" class="col-md-2 control-label">Комментарий:</label>
                    <input type="hidden" value="{{ $post->id }}" name="post_id">
                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control" name="text" required autofocus>
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endif

@if(!empty($comments->count()))
    @include('comment')
@endif

@endsection