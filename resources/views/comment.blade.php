<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h2>Комментарии пользователей:</h2>
            @foreach($comments as $comment)
                <div class="well">
                    {{ $comment->text }}
                    <hr>
                    <small>{{ $comment->user()->name }} @ {{ $comment->created_at }}</small>
                    @if (Auth::check() && 'admin' == Auth::user()->group)
                        <a href="/comment/delete/{{ $comment->id }}" class="btn btn-danger">удалить</a>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>