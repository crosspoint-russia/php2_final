@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12"><h1>Управление публикациями</h1></div>
</div>


<div class="container">


    @if(!empty($posts->count()))
        @include('admin.post_table')
    @else
        <div class="col-md-12">
            <h4>У пользователя <b>{{ $user->name }}</b> нет публикаций</h4>
            <p>Вернуться к <a href="/admin/users">управлению пользователями</a></p>
        </div>
    @endif

</div>

@endsection
