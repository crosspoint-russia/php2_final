@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12"><h1>Управление Пользователями</h1></div>
    </div>


    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Редактирование пользователей</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Пользователь</th>
                                <th>Создан</th>
                                <th>Обновлен</th>
                                <th>Статус</th>
                                <th>Управление</th>
                            </tr>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->updated_at }}</td>
                                    <td>
                                        {{ $user->group }}
                                    </td>
                                    <td>
                                        @if($user->trashed())
                                            <span class="label label-danger">Пользователь заблокирован</span><br/>
                                            <a href="/admin/user/restore/{{ $user->id }}">восстановить</a>
                                        @else
                                            <a href="/admin/user/posts/{{ $user->id }}">все публикации</a><br/>
                                            <a href="/admin/user/comments/{{ $user->id }}">все комментарии</a><br/>
                                            <a href="/admin/user/delete/{{ $user->id }}">удалить пользователя</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('auth.form', ['action' => '/admin/user/add'])

@endsection
