@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Добавление поста</div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/admin/update">

                            @if(isset($post->id) && 'admin' == Auth::user()->group)
                                <input type="hidden" name="id" value="{{ $post->id }}">
                            @endif

                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Заголовок</label>

                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="title" value="{{ $post->title or '' }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Текст</label>

                                <div class="col-md-8">
                                    <textarea name="text" class="form-control" cols="30" rows="10" required>{{ $post->text or '' }}</textarea>

                                    @if ($errors->has('text'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="active" {{ (!empty($post->active)) ? 'checked' : '' }}> Опубликовать
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Отправить
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection