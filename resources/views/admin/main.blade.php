@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-12"><h1>Управление публикациями</h1></div>
    </div>


    <div class="container">

    @include('admin.post_table')

        <div class="row">
            <div class="col-md-12">{{ $posts->links() }}</div>
        </div>

    </div>

@endsection
