<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Редактирование постов</div>

            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Наименование</th>
                        <th>Пользователь</th>
                        <th>Создано</th>
                        <th>Обновлено</th>
                        <th>Статус</th>
                        <th>Управление</th>
                    </tr>
                    @foreach ($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->title }}</td>
                            <td>{{ $post->user()->name }}</td>
                            <td>{{ $post->created_at }}</td>
                            <td>{{ $post->updated_at }}</td>
                            @if(1 == $post->active)
                                <td><span class="label label-success">опубликовано</span></td>
                            @else
                                <td><span class="label label-danger">черновик</span></td>
                            @endif
                            <td><a href="/admin/edit/{{ $post->id }}">редактировать</a><br/><a
                                        href="/admin/delete/{{ $post->id }}">удалить</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>