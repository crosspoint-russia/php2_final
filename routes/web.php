<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// стандартные маршруты для авторизации
Auth::routes();

// основные маршруты публичной части

Route::get('/', 'PostController@index');
Route::get('/show/{id}', 'PostController@show');
Route::get('/user/{id}', 'UserController@show');

// маршруты, куда мы не пускаем гостей

Route::group(['middleware' => ['auth']], function () {

    // маршруты для публикаций

    Route::group(['prefix' => 'post'], function () {
        Route::get('/add', 'PostController@form');
        Route::post('/add', 'PostController@store');
    });

    // маршруты для комментариев

    Route::group(['prefix' => 'comment'], function () {
        Route::post('/add', 'CommentController@store');
        Route::get('/delete/{id}', 'CommentController@delete');
    });

    // маршруты для админ панели включая подгруппы

    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'AdminController@index');
        Route::get('/delete/{id}', 'AdminController@delete');
        Route::get('/edit/{id}', 'AdminController@edit');
        Route::post('/update', 'AdminController@update');
        Route::get('/users', 'AdminUserController@index');
        Route::post('/user/add', 'AdminUserController@add');

        Route::group(['prefix' => 'user'], function () {
            Route::get('/delete/{id}', 'AdminUserController@delete');
            Route::get('/restore/{id}', 'AdminUserController@restore');
            Route::get('/posts/{id}', 'AdminUserController@posts');
            Route::get('/comments/{id}', 'AdminUserController@comments');
        });


    });

});


