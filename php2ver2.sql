-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 10 2017 г., 20:38
-- Версия сервера: 5.5.50
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `php2ver2`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `text`, `user_id`, `post_id`, `created_at`, `updated_at`) VALUES
(10, 'Проверка публикации комментария', 1, 1, '2017-02-10 14:35:48', '2017-02-10 14:35:48'),
(11, 'Тревожные новости!', 7, 1, '2017-02-10 14:37:22', '2017-02-10 14:37:22');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_02_10_074621_create_posts_table', 1),
(4, '2017_02_10_074631_create_comments_table', 1),
(5, '2017_02_10_080751_AddUserGroup', 1),
(6, '2017_02_10_155511_UserSoftDelete', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `text`, `active`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'В Генштабе Турции заявили о передаче России координат попавших под удар военных', 'Генштаб Турции заявил, что заблаговременно оповестил Москву о нахождении своих военных в точке, куда был нанесен удар российскими Воздушно-космическими силами, передает Reuters.\r\n\r\nПо версии Анкары, турецкие военные находились на этой позиции в течение 10 дней. Соответствующие данные за день до инцидента были переданы российским военным на авиабазе в Хмейним, а также российском военному атташе.\r\n\r\nРанее в Кремле сообщили, что в ходе ударов, один из которых повлек гибель трех турецких военных в районе города Аль-Баб, ВКС опирались на данные Анкары. Официальный представитель Кремля подчеркнул, что в Москве причиной непреднамеренного удара считают «несогласованность в предоставлении координат». По его словам, договоренности о совершенствовании механизмов координации поможет в дальнейшем избежать подобных инцидентов.\r\n\r\n9 февраля, в Генштабе ВС Турции сообщили об ударе ВКС России на севере Сирии, в результате которого погибли трое турецких военнослужащих, еще 11 пострадали. Данные подтвердили в Минобороны России, подчеркнув, что удар был непреднамеренным.\r\n\r\nПосле инцидента состоялся телефонный разговор президента России Владимира Путина с турецким лидером Реджепом Тайипом Эрдоганом. Стороны договорились усилить координацию военных действий в Сирии против террористов.\r\n\r\nПозднее источник в военном командовании Турции отметил, что Анкара положительно оценила конструктивную позицию властей России.', 1, 1, '2017-02-10 12:22:18', '2017-02-10 14:14:34'),
(2, 'Командующего ВВС Литвы уволили из-за решения отремонтировать Ми-8 в России', 'Командующий Военно-воздушными силами Литвы Аудронис Навицкас уволен с занимаемой должности из-за намерения отремонтировать вертолеты Ми-8 в России. Об этом в пятницу, 10 февраля, сообщает портал Delfi.\r\n\r\nТакое решение министр обороны республики Раймундас Кароблис принял по результатам служебного расследования. Командующий ВВС был отправлен во временный резерв с перспективой увольнения в запас из рядов армии.\r\n\r\nСообщается, что полковник Навицкас не обеспечил реализации политической установки литовского Минобороны — не ремонтировать военные вертолеты в России. В 2016-м командующий ВВС вместе с другими должностными лицами разрешил исполнить заказ военного ведомства компании, которая собиралась отправить Ми-8 на ремонт в Россию, уточняет издание.', 0, 7, '2017-02-10 14:36:48', '2017-02-10 14:36:48'),
(3, 'Почему россияне за два часа возненавидели лучшего биатлониста мира Фуркада', 'В первой гонке биатлонного чемпионата мира сборная России заняла третье место, с ходу превзойдя результат прошлогоднего первенства, но запомнилась смешанная эстафета не этим. Спортивный мир взбудоражил очередной скандал. «Лента.ру» собрала воедино перипетии вчерашнего дня в австрийском Хохфильцене и вспомнила предыдущие конфликтные ситуации, в которых оказывался замешан лучший, пожалуй, биатлонист современности — француз Мартен Фуркад, который за два часа из любимца российских болельщиков превратился в главного врага.\r\n\r\nПотеря уважения\r\n\r\nПервым намеком на то, что биатлонный вечер в Хохфильцене перестает быть томным, стал эпизод, произошедший перед последним этапом эстафеты. «Любимый» спортсмен Фуркада — россиянин Александр Логинов — завершал свою часть дистанции, отправляя в бой Антона Шипулина. Француз в тот же момент выходил на трассу и, как хорошо видно на видеозаписи, все его внимание было сосредоточено на отбывшем дисквалификацию за допинг оппоненте. После пересмотра ролика мало у кого остались сомнения, что Фуркад намеренно подрезал закончившего забег Логинова, наступив тому на лыжу. Россиянин в результате маневра Мартена упал. При этом сам француз в интервью «Советскому спорту» всячески отрицал свою вину.', 0, 7, '2017-02-10 14:37:08', '2017-02-10 14:37:08');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group` enum('admin','author') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'author',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `group`, `deleted_at`) VALUES
(1, 'Тимур', 'it@crosspointrussia.ru', '$2y$10$DD5wNXwWmVfU/5oQ3lrPFO4y1wOMWxZGrOs6k79VsCGpgw1yl/fPa', 'bmAoGmXKxAooBOXMovNuouVt5Mpy7dZHfuUIYakt9CeoEZSoIG8GDOi6OGA8', '2017-02-10 12:21:33', '2017-02-10 12:21:33', 'admin', NULL),
(7, 'Второй пользователь', 'second@user.ru', '$2y$10$9Vj3Wf1xzPGYNoegJF6S5OUkoozyH3N/zZSMvYeB49JsVuo3QSGtK', NULL, '2017-02-10 14:36:11', '2017-02-10 14:36:11', 'author', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
