<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // связь с публикациями

    public function posts()
    {
        return $this->hasMany('App\Posts', 'user_id')->where('active', 1);
    }

    // связь с комментариями

    public function comments()
    {
        return $this->hasMany('App\Comments', 'user_id');
    }

    // доп метод для проверки принадлежности пользователя к группе администраторов

    public function isAdmin()
    {
        if ('admin' === $this->group) {
            return true;
        } else {
            return false;
        }
    }
}
