<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{

    // связь с автором

    public function user()
    {
        return User::where('id', $this->user_id)->first();
    }

    // связь с публикацией

    public function post()
    {
        return Posts::where('id', $this->post_id)->first();
    }

}
