<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

// наследуемся от промежуточного контроллера который проверяет права пользователя на доступ к админ панели

class AdminUserController extends AdminBaseController
{
    // экшн для прказа таблицы пользователей вместе с soft deleted записями

    public function index()
    {
        $users = User::withTrashed()->orderBy('created_at', 'desc')->paginate(10);;
        return view('admin.users')->with('users', $users);
    }

    // добавление пользователя из админ панели

    public function add(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        return redirect('/admin/users');
    }

    // отображение всех постов пользователя

    public function posts($id)
    {
        $user = User::findOrFail($id);
        $posts = $user->posts()->get();
        return view('admin.user_posts')->withPosts($posts)->withUser($user);
    }

    // отображение всех комментариев пользователя 

    public function comments($id)
    {
        $user = User::findOrFail($id);
        $comments = $user->comments()->get();
        return view('admin.user_comments')->withComments($comments)->withUser($user);
    }

    // удаление пользователя

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back();
    }

    // восстановление пользователя 

    public function restore($id)
    {
        User::withTrashed()
            ->where('id', $id)
            ->restore();
        return redirect()->back();
    }

}
