<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;

class AdminController extends AdminBaseController
{

    // экшн для отображения главной страницы админ панели

    public function index()
    {
        $posts = Posts::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.main')->with('posts', $posts);
    }

    // экшн редактирования публикации

    public function edit($id)
    {
        $post = Posts::findOrFail($id);
        return view('admin.form')->withPost($post);
    }

    // экшн для обновления публикации

    public function update(Request $request)
    {
        $post = Posts::findOrFail($request->get('id'));

        $post->fill($request->all());

        if ($request->has('active')) {
            $post->active = 1;
        } else {
            $post->active = 0;
        }

        $post->save();
        return redirect('/admin');
    }

    // экшн удаления публикации

    public function delete($id)
    {
        $post = Posts::findOrFail($id);
        $post->delete();
        return redirect('/admin');
    }

}
