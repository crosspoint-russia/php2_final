<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    // показываем профиль пользователя в публичной части

    public function show($id)
    {
        $user = User::findOrFail($id);
        $posts = $user->posts()->get();

        return view('show_user')->withPosts($posts)->withUser($user);
    }
}
