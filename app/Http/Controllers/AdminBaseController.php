<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 10.02.2017
 * Time: 20:03
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;

class AdminBaseController extends Controller
{
    public function __construct()
    {
        // в конструкторе базового админ-контроллера проверяем есть ли у пользователя
        // доступ к админ панели. Если нет - делаем редирект на главную.
        // Если доуступ есть, передаем управление дальше.

        $this->middleware(function ($request, $next) {

            if (!Auth::user()->isAdmin()) {
                return redirect('/');
            }

            return $next($request);
        });
    }


}