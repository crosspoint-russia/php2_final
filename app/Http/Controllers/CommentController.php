<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    // сохранение комментария

    public function store(Request $request)
    {
        $comment = new Comments();
        $comment->text = $request->get('text');
        $comment->post_id = $request->get('post_id');
        $comment->user_id = $request->user()->id;

        $comment->save();

        return redirect('/show/' . $comment->post_id);

    }

    // удаление комментария

    public function delete($id)
    {
        $comment = Comments::findOrFail($id);
        $comment->delete();

        return redirect()->back();
    }
}
