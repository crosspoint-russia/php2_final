<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;

class PostController extends Controller
{

    // базовый контроллер для отображения главной страницы блога

    public function index()
    {
        $posts = Posts::where('active', 1)->orderBy('created_at', 'desc')->paginate(5);

        return view('home')->with('posts', $posts);
    }

    public function form($id = null)
    {
        if (!is_null($id)) {
            $post = Posts::findOrFail($id);
            return view('post_form')->with('post', $post);
        }
        return view('post_form');
    }

    // сохранение поста в бд

    public function store(Request $request)
    {
        $pub = new Posts();
        $pub->title = $request->get('title');
        $pub->text = $request->get('text');
        $pub->user_id = $request->user()->id;

        if ($request->has('active')) {
            $pub->active = 1;
        } else {
            $pub->active = 0;
        }

        $pub->save();

        return redirect('/');
    }

    // контроллер для отображения записи блога

    public function show($id)
    {
        $post = Posts::findOrFail($id);
        $comments = $post->comments()->orderBy('created_at', 'desc')->get();

        return view('show_post')->withPost($post)->withComments($comments);
    }
}
