<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{

    // заполняемые свойства

    protected $fillable = [
        'id', 'title', 'text',
    ];

    // связь с автором

    public function user()
    {
        return User::where('id', $this->user_id)->withTrashed()->first();
    }

    // связь с комментариями

    public function comments()
    {
        return $this->hasMany('App\Comments', 'post_id');
    }
}
